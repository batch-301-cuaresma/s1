package com.zuitt.example;

import java.awt.*;
import java.util.Scanner;

public class UserInput {
    public static  void main(String[] args){

        Scanner myObj = new Scanner(System.in); // Create a scanner object

        System.out.println("Enter first name: ");
        String firstName = myObj.nextLine(); // Read user input with use of the user
        System.out.println("Enter last name: ");
        String lastName = myObj.nextLine();
        System.out.println("My name is " + firstName + " " + lastName);
    }
}
