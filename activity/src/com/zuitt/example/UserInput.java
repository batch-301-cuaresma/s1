package com.zuitt.example;

import java.util.Scanner;

public class UserInput {
    public static void main(String[] args){
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;
        double average;
        Scanner myObj = new Scanner(System.in);

        System.out.println("First Name:");
        firstName = myObj.nextLine();
        System.out.println("Last Name:");
        lastName = myObj.nextLine();
        System.out.println("First Subject Grade:");
        firstSubject = new Double(myObj.nextLine());
        System.out.println("Second Subject Grade:");
        secondSubject = new Double(myObj.nextLine());
        System.out.println("Third Subject Grade:");
        thirdSubject = new Double(myObj.nextLine());
        average = (firstSubject + secondSubject + thirdSubject) / 3;
        System.out.println("My name is " + firstName + " " + lastName+ ".");
        System.out.println("My average grade is: " + average );





    }
}
